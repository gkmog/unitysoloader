package com.gameskraft.unitysoloader;

import android.app.Application;

import androidx.multidex.MultiDexApplication;

import com.gameskraft.unitysoloader.helper.LibManager;

public class MainApplication extends MultiDexApplication {
    public static Application instance;

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
        new LibManager(this);
    }
}
