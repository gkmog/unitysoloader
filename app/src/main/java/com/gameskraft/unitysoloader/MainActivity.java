package com.gameskraft.unitysoloader;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.os.Process;

import com.facebook.soloader.DirectorySoSource;
import com.facebook.soloader.SoLoader;
import com.facebook.soloader.SoSource;
import com.gameskraft.unitysoloader.helper.LibManager;
import com.unity3d.player.IUnityPlayerLifecycleEvents;
import com.unity3d.player.UnityPlayer;

import java.io.File;

public class MainActivity extends Activity implements IUnityPlayerLifecycleEvents
{

        static {
        System.out.println("## Call inside static " + MainApplication.instance.getFilesDir());

//        Copy libmain.so and libunity.so to /storage/emulated/0/Android/data/com.gameskraft.unitysoloader/
//        Can be done using adb push <pathTOLibmainLibunity> /storage/emulated/0/Android/data/com.gameskraft.unitysoloader/
        String downloadedFileLocation = "/storage/emulated/0/Android/data/com.gameskraft.unitysoloader/";
        File privateLocation = MainApplication.instance.getDir("lib", Activity.MODE_PRIVATE);

        String[] libNames = {"libunity.so","libil2cpp.so"};
        SoLoader.init(MainApplication.instance, false);
        try {
            for (String name : libNames) {
                File sourceFolder = new File(downloadedFileLocation, name);
                File destFolder = new File(privateLocation, name);

                boolean isCopied = LibManager.copyFileFromAssets(sourceFolder, destFolder.getAbsolutePath());
                System.out.println("## sourceFolder :: " + sourceFolder.getAbsolutePath());
                System.out.println("## destFolder :: " + destFolder.getAbsolutePath());
                System.out.println("## is file copied :: " + isCopied);
            }

            DirectorySoSource soSource = new DirectorySoSource(privateLocation, SoSource.LOAD_FLAG_ALLOW_IMPLICIT_PROVISION);
            SoLoader.prependSoSource(soSource);
//            SoLoader.loadLibrary("main");
            SoLoader.loadLibrary("unity");
            SoLoader.loadLibrary("il2cpp");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

    protected String updateUnityCommandLineArguments(String cmdLine)
    {
        return cmdLine;
    }

    // Setup activity layout
    @Override protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        String cmdLine = updateUnityCommandLineArguments(getIntent().getStringExtra("unity"));
        getIntent().putExtra("unity", cmdLine);

        mUnityPlayer = new UnityPlayer(this, this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();
    }

    // When Unity player unloaded move task to background
    @Override public void onUnityPlayerUnloaded() {
        moveTaskToBack(true);
    }

    // When Unity player quited kill process
    @Override public void onUnityPlayerQuitted() {
        Process.killProcess(Process.myPid());
    }

    @Override protected void onNewIntent(Intent intent)
    {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
        mUnityPlayer.newIntent(intent);
    }

    // Quit Unity
    @Override protected void onDestroy ()
    {
        mUnityPlayer.destroy();
        super.onDestroy();
    }

    // Pause Unity
    @Override protected void onPause()
    {
        super.onPause();
        mUnityPlayer.pause();
    }

    // Resume Unity
    @Override protected void onResume()
    {
        super.onResume();
        mUnityPlayer.resume();
    }

    // Low Memory Unity
    @Override public void onLowMemory()
    {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    // Trim Memory Unity
    @Override public void onTrimMemory(int level)
    {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL)
        {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }
}
