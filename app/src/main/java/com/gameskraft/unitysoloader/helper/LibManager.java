package com.gameskraft.unitysoloader.helper;


import android.app.Application;
import android.util.Log;

import com.gameskraft.unitysoloader.MainApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

public class LibManager {
    private static LibManager instance;
    private final Application application;

    private static final String TAG = "LibManager";

    private static boolean libsLoadedOnce = false;

    public LibManager(Application context) {
        application = context;
    }

    public static LibManager getInstance() {
        if (instance == null) {
            instance = new LibManager(MainApplication.instance);
        }
        return instance;
    }


    public static boolean copyFileFromAssets(File sourceFile, String path) throws Exception {
        Log.d(TAG, "copyFileFromAssets: " + path);
        boolean copyIsFinish = false;
        try {
            InputStream is = new FileInputStream(sourceFile);
            File file = new File(path);
            file.createNewFile();
            chmod(file, 0755);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] temp = new byte[1024];
            int i = 0;
            while ((i = is.read(temp)) > 0) {
                fos.write(temp, 0, i);
            }
            fos.close();
            is.close();
            copyIsFinish = true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "[copyFileFromAssets] IOException " + e.toString());
        }
        return copyIsFinish;
    }

    public static int chmod(File path, int mode) throws Exception {
        Class fileUtils = Class.forName("android.os.FileUtils");
        Method setPermissions = fileUtils.getMethod("setPermissions",
                String.class, int.class, int.class, int.class);
        return (Integer) setPermissions.invoke(null, path.getAbsolutePath(), mode, -1, -1);
    }
}
